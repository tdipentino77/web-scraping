import time
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

class VoteBot():
  def __init__(self):  
    s=Service(ChromeDriverManager().install())
    self.driver = webdriver.Chrome(service=s)

  def vote(self):    
    self.driver.get('https://www.coloradobrewerylist.com/colorado-brewery-madness-2022/')
    launch_pad = self.driver.find_element(by=By.XPATH, value="//label[@for='Total_Soft_Poll_1_Ans_23_2']")
    launch_pad.click()
    vote_for = self.driver.find_element(by=By.XPATH, value="//button[@id='Total_Soft_Poll_1_But_Vote_23']")
    vote_for.click()
    time.sleep(3)
    self.driver.close()

for x in range(70, 0, -1):
  print("To infinity and beyond! We're getting close, only %d left!" % (x))
  bot = VoteBot()
  bot.vote()